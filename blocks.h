//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "mem.sh",		3, 0},

	{"", "cpu.sh",		3, 0},

	{"", "net.sh",		10, 0},

	{"", "volume.sh",	3, 10},

	{"", "dte.sh",		60, 0}

};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
