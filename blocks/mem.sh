#/bin/bash

mem="$(free -h | grep '^Mem' | awk '{print "[mem: " $3 " / " $2}')]"
echo -e "$mem"
